package com.cola.colaboot.module.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cola.colaboot.module.system.mapper.SysUserMapper;
import com.cola.colaboot.module.system.service.SysUserService;
import com.cola.colaboot.module.system.pojo.SysUser;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    @Override
    public IPage<SysUser> pageList(SysUser sysUser, Integer pageNo, Integer pageSize) {
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(sysUser.getUsername()),SysUser::getUsername,sysUser.getUsername());
        queryWrapper.eq(StringUtils.isNotBlank(sysUser.getPhone()),SysUser::getPhone,sysUser.getPhone());
        queryWrapper.orderByDesc(SysUser::getCreateTime);
        Page<SysUser> page = new Page<>(pageNo, pageSize);
        return page(page, queryWrapper);
    }

    @Override
    public SysUser getByUsername(String userName) {
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getUsername,userName);
        return getOne(queryWrapper);
    }

    @Override
    public SysUser getByPhone(String phone) {
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getPhone,phone);
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }
}

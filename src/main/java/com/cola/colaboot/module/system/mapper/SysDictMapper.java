package com.cola.colaboot.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cola.colaboot.module.system.pojo.SysDict;

public interface SysDictMapper extends BaseMapper<SysDict> {
}
